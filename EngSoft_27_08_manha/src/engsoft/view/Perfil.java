/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engsoft.view;
import dao.Alunodao;
import java.awt.Color;
import java.awt.Event;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import model.Aluno;
/**
 *
 * @author vinicius
 */
public class Perfil extends javax.swing.JFrame {
Aluno b ;

    
    
    public Perfil(Aluno a) {
        b=a;
        initComponents();
       Titulo.setText("Bem vindo "+a.getLogin());
       if(a.getNome()!=null){
           TextNome.setText("Nome: "+a.getNome());
           
       }
       textUsu.setText("Usuário: "+a.getLogin());
       if(a.getAltura()!=0&&a.getPeso()!=0){
           Altura.setText("Altura: "+a.getAltura());
            Peso.setText("Peso: "+a.getPeso());
       }else{
             Altura.setText("");
            Peso.setText("");
       }
 
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
       


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        DadosUsu = new javax.swing.JLabel();
        Altsenha = new javax.swing.JLabel();
        Info = new javax.swing.JLabel();
        Inicio = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        Titulo = new javax.swing.JLabel();
        TextNome = new javax.swing.JLabel();
        textUsu = new javax.swing.JLabel();
        Altura = new javax.swing.JLabel();
        Peso = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 410));
        setResizable(false);
        setSize(new java.awt.Dimension(400, 410));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jPanel5.setBackground(new java.awt.Color(242, 242, 242));

        DadosUsu.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        DadosUsu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8-administrador-masculino-24.png"))); // NOI18N
        DadosUsu.setText("Alterar dados de usuário");
        DadosUsu.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        DadosUsu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DadosUsuMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DadosUsuMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DadosUsuMouseExited(evt);
            }
        });
        DadosUsu.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                DadosUsuComponentShown(evt);
            }
        });

        Altsenha.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Altsenha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8-senha-24.png"))); // NOI18N
        Altsenha.setText("Alterar Senha");
        Altsenha.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Altsenha.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AltsenhaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                AltsenhaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                AltsenhaMouseExited(evt);
            }
        });

        Info.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Info.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/addition-sign.png"))); // NOI18N
        Info.setText("Informacoes adicionais");
        Info.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Info.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                InfoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                InfoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                InfoMouseExited(evt);
            }
        });

        Inicio.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Inicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8-pessoa-em-casa-24.png"))); // NOI18N
        Inicio.setText("Inicio");
        Inicio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Inicio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                InicioMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                InicioMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                InicioMouseExited(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/MEKmZWL.png"))); // NOI18N
        jLabel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel1MouseMoved(evt);
            }
        });
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel1MouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Info)
                            .addComponent(DadosUsu)
                            .addComponent(Altsenha)
                            .addComponent(Inicio)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jLabel1)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
                .addComponent(Inicio)
                .addGap(29, 29, 29)
                .addComponent(DadosUsu)
                .addGap(34, 34, 34)
                .addComponent(Altsenha, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(Info)
                .addGap(79, 79, 79))
        );

        Titulo.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        Titulo.setText("Bem vindo Vinicius");
        Titulo.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                TituloInputMethodTextChanged(evt);
            }
        });

        TextNome.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        TextNome.setText("Nome:");

        textUsu.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        textUsu.setText("Usuário:");

        Altura.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Altura.setText("Altura:");

        Peso.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Peso.setText("Peso:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TextNome, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(textUsu, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                            .addComponent(Titulo, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(12, 12, 12)))
                    .addComponent(Altura)
                    .addComponent(Peso))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(Titulo)
                .addGap(35, 35, 35)
                .addComponent(TextNome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textUsu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(35, 35, 35)
                .addComponent(Altura)
                .addGap(53, 53, 53)
                .addComponent(Peso)
                .addGap(77, 77, 77))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    
    private void DadosUsuComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_DadosUsuComponentShown
    
    }//GEN-LAST:event_DadosUsuComponentShown

    private void TituloInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_TituloInputMethodTextChanged
//        Titulo.setText("Bem vindo");
    }//GEN-LAST:event_TituloInputMethodTextChanged

    private void InicioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InicioMouseClicked
        // TODO add your handling code here:
        setVisible(false);
        Inicial I = new Inicial(b);
        I.setVisible(true);
       
    }//GEN-LAST:event_InicioMouseClicked

    private void jLabel1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseMoved
        //        jLabel1.setText("www");
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/pXzLOUi.png"))); // NOI18N);
    }//GEN-LAST:event_jLabel1MouseMoved

    private void jLabel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseExited
        //        jLabel1.setText("");
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/MEKmZWl.png"))); // NOI18N);
    }//GEN-LAST:event_jLabel1MouseExited

    private void AltsenhaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AltsenhaMouseClicked
        // TODO add your handling code here:
        AlterarSenha AS= new AlterarSenha(b);
        AS.setVisible(true);
    }//GEN-LAST:event_AltsenhaMouseClicked

    private void DadosUsuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DadosUsuMouseClicked
        // TODO add your handling code here:
        AlterarLoginNome al = new AlterarLoginNome(b);
        setVisible(false);
        al.setVisible(true);
    }//GEN-LAST:event_DadosUsuMouseClicked

    private void InicioMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InicioMouseEntered
               Inicio.setForeground(new Color(255,255,255));
    }//GEN-LAST:event_InicioMouseEntered

    private void InicioMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InicioMouseExited
        // TODO add your handling code here:
        Inicio.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_InicioMouseExited

    private void DadosUsuMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DadosUsuMouseEntered
               DadosUsu.setForeground(new Color(255,255,255));
    }//GEN-LAST:event_DadosUsuMouseEntered

    private void DadosUsuMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DadosUsuMouseExited
        DadosUsu.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_DadosUsuMouseExited

    private void AltsenhaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AltsenhaMouseEntered
               Altsenha.setForeground(new Color(255,255,255));
    }//GEN-LAST:event_AltsenhaMouseEntered

    private void AltsenhaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AltsenhaMouseExited
        Altsenha.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_AltsenhaMouseExited

    private void InfoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InfoMouseEntered
               Info.setForeground(new Color(255,255,255));
    }//GEN-LAST:event_InfoMouseEntered

    private void InfoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InfoMouseExited
        Info.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_InfoMouseExited

    private void InfoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InfoMouseClicked
        // TODO add your handling code here:
        
        InformacoesAdicionais info = new InformacoesAdicionais(b);
        setVisible(false);
        info.setVisible(true);
    }//GEN-LAST:event_InfoMouseClicked

    /*
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(Perfil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(Perfil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(Perfil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(Perfil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//              
//                new Perfil().setVisible(true);
//                
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Altsenha;
    private javax.swing.JLabel Altura;
    private javax.swing.JLabel DadosUsu;
    private javax.swing.JLabel Info;
    private javax.swing.JLabel Inicio;
    private javax.swing.JLabel Peso;
    private javax.swing.JLabel TextNome;
    private javax.swing.JLabel Titulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel textUsu;
    // End of variables declaration//GEN-END:variables

}
