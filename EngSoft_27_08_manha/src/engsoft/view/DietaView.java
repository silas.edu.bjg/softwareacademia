/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engsoft.view;

import dao.Alunodao;
import java.awt.Color;
import model.Aluno;

/**
 *
 * @author Usuario
 */
public class DietaView extends javax.swing.JFrame {

    /**
     * Creates new form DietaView
     */

    public DietaView() {
        initComponents();
      
    }

  

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        ConsultarDieta = new javax.swing.JLabel();
        pesquisardieta = new javax.swing.JLabel();
        adicionardieta = new javax.swing.JLabel();
        Menu1 = new javax.swing.JLabel();
        Deletardieta1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        sair = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(1600, 900));

        jPanel2.setBackground(new java.awt.Color(242, 242, 242));

        ConsultarDieta.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ConsultarDieta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8-prato-de-sopa-30.png"))); // NOI18N
        ConsultarDieta.setText("Consultar Dieta");
        ConsultarDieta.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        ConsultarDieta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ConsultarDietaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ConsultarDietaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ConsultarDietaMouseExited(evt);
            }
        });

        pesquisardieta.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        pesquisardieta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/magnifier.png"))); // NOI18N
        pesquisardieta.setText("Pesquisar Dieta");
        pesquisardieta.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        pesquisardieta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pesquisardietaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pesquisardietaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pesquisardietaMouseExited(evt);
            }
        });

        adicionardieta.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        adicionardieta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/addition-sign.png"))); // NOI18N
        adicionardieta.setText("Adicionar Dieta");
        adicionardieta.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        adicionardieta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                adicionardietaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                adicionardietaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                adicionardietaMouseExited(evt);
            }
        });

        Menu1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Menu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8-pessoa-em-casa-24.png"))); // NOI18N
        Menu1.setText("Inicio");
        Menu1.setAlignmentX(0.5F);
        Menu1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Menu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                Menu1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                Menu1MouseExited(evt);
            }
        });

        Deletardieta1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Deletardieta1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/rubbish-bin.png"))); // NOI18N
        Deletardieta1.setText("Deletar Dieta");
        Deletardieta1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Deletardieta1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Deletardieta1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                Deletardieta1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                Deletardieta1MouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(adicionardieta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pesquisardieta, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(Menu1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(ConsultarDieta, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Deletardieta1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Menu1)
                .addGap(61, 61, 61)
                .addComponent(pesquisardieta, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(ConsultarDieta)
                .addGap(61, 61, 61)
                .addComponent(adicionardieta)
                .addGap(63, 63, 63)
                .addComponent(Deletardieta1)
                .addContainerGap(552, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(242, 242, 242));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 44, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 113, Short.MAX_VALUE)
        );

        jPanel4.setBackground(new java.awt.Color(242, 242, 242));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/MEKmZWL.png"))); // NOI18N
        jLabel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel1MouseMoved(evt);
            }
        });
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel1MouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(48, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(26, 26, 26))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(21, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(126, 126, 126)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap(1133, Short.MAX_VALUE)))
        );

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/trazabilidad_alimentos.jpg"))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel3.setText("DIETAS");

        sair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/door-exit.png"))); // NOI18N
        sair.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        sair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sairMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(475, 475, 475)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(sair)
                .addGap(60, 60, 60))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 1144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jLabel3))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(sair)))
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 830, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ConsultarDietaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ConsultarDietaMouseClicked
//        JOptionPane.showMessageDialog(null, "Usuario nao encontrado");
    }//GEN-LAST:event_ConsultarDietaMouseClicked

    private void ConsultarDietaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ConsultarDietaMouseEntered
        ConsultarDieta.setForeground(new Color(255,255,255));
    }//GEN-LAST:event_ConsultarDietaMouseEntered

    private void ConsultarDietaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ConsultarDietaMouseExited
        ConsultarDieta.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_ConsultarDietaMouseExited

    private void adicionardietaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_adicionardietaMouseClicked
        // TODO add your handling code here:
        AdicionarDieta adc = new AdicionarDieta();
        setVisible(false);
        adc.setVisible(true);
    }//GEN-LAST:event_adicionardietaMouseClicked

    private void adicionardietaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_adicionardietaMouseEntered
        adicionardieta.setForeground(new Color(255,255,255));
    }//GEN-LAST:event_adicionardietaMouseEntered

    private void adicionardietaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_adicionardietaMouseExited
        adicionardieta.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_adicionardietaMouseExited

    private void pesquisardietaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pesquisardietaMouseExited
        pesquisardieta.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_pesquisardietaMouseExited

    private void pesquisardietaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pesquisardietaMouseEntered
        pesquisardieta.setForeground(new Color(255,255,255));
    }//GEN-LAST:event_pesquisardietaMouseEntered

                                          

    private void pesquisardietaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pesquisardietaMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_pesquisardietaMouseClicked

    private void sairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sairMouseClicked
         sair.setForeground(new Color(255,255,255));
        setVisible(false);
        Tela f = new Tela();
        f.setVisible(true);
    }//GEN-LAST:event_sairMouseClicked

    private void jLabel1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseMoved
          jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/pXzLOUi.png"))); // NOI18N);
    }//GEN-LAST:event_jLabel1MouseMoved

    private void jLabel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseExited
        // TODO add your handling code here:
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/MEKmZWl.png"))); // NOI18N);
    }//GEN-LAST:event_jLabel1MouseExited

    private void Menu1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Menu1MouseEntered
        Menu1.setForeground(new Color(255,255,255));
    }//GEN-LAST:event_Menu1MouseEntered

    private void Menu1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Menu1MouseExited
        Menu1.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_Menu1MouseExited

    private void Deletardieta1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Deletardieta1MouseClicked
        // TODO add your handling code here:
        DeletarDieta1 del = new DeletarDieta1();
        setVisible(false);
        del.setVisible(true);
        
    }//GEN-LAST:event_Deletardieta1MouseClicked

    private void Deletardieta1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Deletardieta1MouseEntered
        // TODO add your handling code here:
                Deletardieta1.setForeground(new Color(255,255,255));

    }//GEN-LAST:event_Deletardieta1MouseEntered

    private void Deletardieta1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Deletardieta1MouseExited
        // TODO add your handling code here:
                Deletardieta1.setForeground(new Color(0,0,0));

    }//GEN-LAST:event_Deletardieta1MouseExited

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(DietaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(DietaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(DietaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(DietaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new DietaView().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ConsultarDieta;
    private javax.swing.JLabel Deletardieta1;
    private javax.swing.JLabel Menu1;
    private javax.swing.JLabel adicionardieta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel pesquisardieta;
    private javax.swing.JLabel sair;
    // End of variables declaration//GEN-END:variables
}
