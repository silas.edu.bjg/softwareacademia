/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engsoft.view;

import dao.Alunodao;
import dao.Treinodao;
import java.awt.Color;
import javax.swing.JOptionPane;
import model.Aluno;
import model.TreinoModel;

/**
 *
 * @author vinicius
 */
public class Treinooo extends javax.swing.JFrame {

    /**
     * Creates new form Treinooo
     */
    Aluno b;
    Alunodao dao;
    TreinoModel Treino;
    Treinodao Treinodao;
    public Treinooo(Aluno a) {
        b=a;
        initComponents();
        jPanel3.setVisible(true);
        jPanel4.setVisible(false);
        DESCRICAO.setBorder(null);
 DESCRICAO.setOpaque(false);
 jScrollPane1.setBorder(null);
  jScrollPane1.getViewport().setOpaque(false);
 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        Logo = new javax.swing.JLabel();
        Menu = new javax.swing.JLabel();
        AdicionarTreino = new javax.swing.JLabel();
        ConsultarTreino = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        PesquisarTreino = new javax.swing.JLabel();
        Pesquisar = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        Sair = new javax.swing.JLabel();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        jPanel3 = new javax.swing.JPanel();
        nopain = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        OBJETIVO = new javax.swing.JLabel();
        NOME = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        DESCRICAO = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Cadastrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(new java.awt.Dimension(1600, 900));

        jPanel1.setBackground(new java.awt.Color(242, 242, 242));

        Logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/MEKmZWL.png"))); // NOI18N
        Logo.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                LogoMouseMoved(evt);
            }
        });
        Logo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LogoMouseExited(evt);
            }
        });

        Menu.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Menu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8-pessoa-em-casa-24.png"))); // NOI18N
        Menu.setText("Inicio");
        Menu.setAlignmentX(0.5F);
        Menu.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Menu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MenuMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                MenuMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                MenuMouseExited(evt);
            }
        });

        AdicionarTreino.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        AdicionarTreino.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/addition-sign.png"))); // NOI18N
        AdicionarTreino.setText("Adicionar Treino");
        AdicionarTreino.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        AdicionarTreino.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AdicionarTreinoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                AdicionarTreinoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                AdicionarTreinoMouseExited(evt);
            }
        });

        ConsultarTreino.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ConsultarTreino.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/HALTER.png"))); // NOI18N
        ConsultarTreino.setText("Consultar treinos");
        ConsultarTreino.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        ConsultarTreino.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ConsultarTreinoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ConsultarTreinoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ConsultarTreinoMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(Logo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ConsultarTreino, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Menu, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AdicionarTreino, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(Logo)
                .addGap(40, 40, 40)
                .addComponent(Menu, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(AdicionarTreino, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(ConsultarTreino, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(596, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        PesquisarTreino.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        PesquisarTreino.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/magnifier.png"))); // NOI18N
        PesquisarTreino.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        PesquisarTreino.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PesquisarTreinoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                PesquisarTreinoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                PesquisarTreinoMouseExited(evt);
            }
        });

        Pesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PesquisarActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel3.setText("TREINOS");

        Sair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/door-exit.png"))); // NOI18N
        Sair.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Sair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SairMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(86, 86, 86)
                .addComponent(PesquisarTreino)
                .addGap(18, 18, 18)
                .addComponent(Pesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(249, 249, 249)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Sair)
                .addGap(75, 75, 75))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(62, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(PesquisarTreino, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Pesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(Sair)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel3)))
                .addGap(30, 30, 30))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        nopain.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/NO-PAIN-NO-GAIN1.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(nopain, javax.swing.GroupLayout.PREFERRED_SIZE, 1326, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(680, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(164, Short.MAX_VALUE)
                .addComponent(nopain)
                .addGap(32, 32, 32))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        OBJETIVO.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        OBJETIVO.setText("Birl");

        NOME.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        NOME.setText("Birl");

        jScrollPane1.setOpaque(false);

        DESCRICAO.setColumns(20);
        DESCRICAO.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        DESCRICAO.setRows(5);
        DESCRICAO.setBorder(null);
        DESCRICAO.setSelectionColor(new java.awt.Color(255, 255, 255));
        jScrollPane1.setViewportView(DESCRICAO);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/photo-1526506118085-60ce8714f8c5.jpg"))); // NOI18N

        Cadastrar.setBackground(new java.awt.Color(217, 81, 51));
        Cadastrar.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        Cadastrar.setForeground(new java.awt.Color(255, 255, 255));
        Cadastrar.setText("Adicionar treino ao perfil");
        Cadastrar.setBorderPainted(false);
        Cadastrar.setFocusPainted(false);
        Cadastrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CadastrarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                CadastrarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                CadastrarMouseExited(evt);
            }
        });
        Cadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CadastrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(98, 98, 98)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(NOME, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OBJETIVO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE)
                    .addComponent(Cadastrar, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE))
                .addGap(65, 65, 65)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addContainerGap(708, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(NOME, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(OBJETIVO, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(78, 78, 78)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(Cadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(jLabel2)))
                .addContainerGap(579, Short.MAX_VALUE))
        );

        jLayeredPane2.setLayer(jPanel3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jPanel4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane2Layout = new javax.swing.GroupLayout(jLayeredPane2);
        jLayeredPane2.setLayout(jLayeredPane2Layout);
        jLayeredPane2Layout.setHorizontalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane2Layout.createSequentialGroup()
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jLayeredPane2Layout.setVerticalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane2Layout.createSequentialGroup()
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLayeredPane2)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLayeredPane2))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LogoMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LogoMouseMoved
        //        jLabel1.setText("www");
        Logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/pXzLOUi.png"))); // NOI18N);
    }//GEN-LAST:event_LogoMouseMoved

    private void LogoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LogoMouseExited
        //        jLabel1.setText("");
        Logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/MEKmZWl.png"))); // NOI18N);
    }//GEN-LAST:event_LogoMouseExited

    private void MenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MenuMouseClicked
        // TODO add your handling code here:
        //        Nopain.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/NO-PAIN-NO-GAIN1.jpg"))); // NOI18N);
        Inicial I = new Inicial(b);
        setVisible(false);
        I.setVisible(true);
    }//GEN-LAST:event_MenuMouseClicked

    private void MenuMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MenuMouseEntered
        Menu.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_MenuMouseEntered

    private void MenuMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MenuMouseExited
        Menu.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_MenuMouseExited

    private void AdicionarTreinoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AdicionarTreinoMouseClicked
        // TODO add your handling code here:
        AdicionarTreino AT = new AdicionarTreino(b);
        setVisible(false);
        AT.setVisible(true);
    }//GEN-LAST:event_AdicionarTreinoMouseClicked

    private void AdicionarTreinoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AdicionarTreinoMouseEntered
        AdicionarTreino.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_AdicionarTreinoMouseEntered

    private void AdicionarTreinoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AdicionarTreinoMouseExited
        AdicionarTreino.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_AdicionarTreinoMouseExited

    private void ConsultarTreinoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ConsultarTreinoMouseClicked
        //        JOptionPane.showMessageDialog(null, "Usuario nao encontrado");
    }//GEN-LAST:event_ConsultarTreinoMouseClicked

    private void ConsultarTreinoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ConsultarTreinoMouseEntered
        ConsultarTreino.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_ConsultarTreinoMouseEntered

    private void ConsultarTreinoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ConsultarTreinoMouseExited
        ConsultarTreino.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_ConsultarTreinoMouseExited

    private void PesquisarTreinoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PesquisarTreinoMouseClicked
        if (Pesquisar.getText() != null) {

            TreinoModel a = new TreinoModel(Pesquisar.getText());
            TreinoModel b;
            Treinodao daoo = new Treinodao();
            if (daoo.Buscar(a) == true) {
                
                b = daoo.PesquisarnoBD(a);
        NOME.setText("Nome: "+b.getNome());
         
         OBJETIVO.setText("Objetivo: "+b.getObjetivo());
         DESCRICAO.setText("Descricao: "+b.getDescricao());
                 DESCRICAO.setBorder(null);
 DESCRICAO.setOpaque(false);
 jScrollPane1.setBorder(null);
                jPanel3.setVisible(false);
                jPanel4.setVisible(true);
                jScrollPane1.getViewport().setOpaque(false);

        
        
//                Nopain.setIcon(null);

                //              Nopain.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/MEKmZWl.png"))); // NOI18N);
            //           Nopain.setIcon(null); // NOI18N);

        }else{
            JOptionPane.showMessageDialog(null, "Treino nao encontrado" );
        }

        }
    }//GEN-LAST:event_PesquisarTreinoMouseClicked

    private void PesquisarTreinoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PesquisarTreinoMouseEntered

    }//GEN-LAST:event_PesquisarTreinoMouseEntered

    private void PesquisarTreinoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PesquisarTreinoMouseExited
        PesquisarTreino.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_PesquisarTreinoMouseExited

    private void PesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PesquisarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PesquisarActionPerformed

    private void SairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SairMouseClicked
        setVisible(false);
        Tela f = new Tela();
        f.setVisible(true);
    }//GEN-LAST:event_SairMouseClicked

    private void CadastrarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CadastrarMouseEntered
        Cadastrar.setBackground(new Color(235,235,235));
        Cadastrar.setForeground(new Color(217,81,51));
    }//GEN-LAST:event_CadastrarMouseEntered

    private void CadastrarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CadastrarMouseExited
        Cadastrar.setBackground(new Color(217,81,51));
        Cadastrar.setForeground(Color.WHITE);
    }//GEN-LAST:event_CadastrarMouseExited

    private void CadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CadastrarActionPerformed
        CadastroUsuario1 cad = new CadastroUsuario1();
        setVisible(false);
        cad.setVisible(true);

    }//GEN-LAST:event_CadastrarActionPerformed

    private void CadastrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CadastrarMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_CadastrarMouseClicked

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(Treinooo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(Treinooo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(Treinooo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(Treinooo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new Treinooo().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AdicionarTreino;
    private javax.swing.JButton Cadastrar;
    private javax.swing.JLabel ConsultarTreino;
    private javax.swing.JTextArea DESCRICAO;
    private javax.swing.JLabel Logo;
    private javax.swing.JLabel Menu;
    private javax.swing.JLabel NOME;
    private javax.swing.JLabel OBJETIVO;
    private javax.swing.JTextField Pesquisar;
    private javax.swing.JLabel PesquisarTreino;
    private javax.swing.JLabel Sair;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nopain;
    // End of variables declaration//GEN-END:variables
}
